import { s__ } from '~/locale';

export const MAX_LIST_COUNT = 25;

export const I18N_AVAILABLE_AGENTS_DROPDOWN = {
  selectAgent: s__('ClusterAgents|Select an Agent'),
  registeringAgent: s__('ClusterAgents|Registering Agent'),
};
